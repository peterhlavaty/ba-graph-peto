#ifndef BA_GRAPH_EDMONDS_KARP_HPP
#define BA_GRAPH_EDMONDS_KARP_HPP

#include <basic_impl.hpp>
#include <stdio.h>
#include <cstdlib>
#include <queue>
#include <climits>
#include <unordered_set>
#include <unordered_map>

namespace ba_graph{
    class EdmondsKarp{
        int vertexCount;
        bool directed;
        Graph* sourceGraph = NULL;
        std::vector<Vertex>* sources = NULL;
        std::vector<Vertex>* sinks = NULL;
        EdgeLabeling<int>* capacities = NULL;

        bool* discoveredVertices;
        int* parentVertex;
        bool finished = false;

        struct edge{
            int vertex;
            int capacity;
            int currentFlow;
            int residualCapacity;
        };

        struct vertex{
            int number;
            std::unordered_map<int,edge*>* next;
        };

        vertex** graph;

        edge* findEdge(int fromVertex, int toVertex){
            auto pair = graph[fromVertex]->next->find(toVertex);
            if(pair!=graph[fromVertex]->next->end()){
                return (*pair).second;
            }
            return NULL;
        }

        int pathVolume(int start, int end, int* parents){
            if (parents[end] == -1) return 0;

            edge* e = findEdge(parents[end], end);
            if (start == parents[end]){
                return e->residualCapacity;
            }
            return std::min(pathVolume(start, parents[end], parents), e->residualCapacity);
        }

        void augmentPath(int start, int end, int* parents, int volume){
            if (start == end) return;

            edge* e = findEdge(parents[end], end);
            e->currentFlow += volume;
            e->residualCapacity -= volume;
            e = findEdge(end, parents[end]);
            e->residualCapacity += volume;
            augmentPath(start, parents[end], parents, volume);
        }

        void initializeGraph(){
            graph = (vertex**) malloc(vertexCount * sizeof(vertex*));
            for (int i=0; i < vertexCount; i++){
                graph[i]=(vertex*)malloc(sizeof(vertex));
                graph[i]->number = i;
                graph[i]->next = new std::unordered_map<int, edge*>;
            }
        }

        void destroyGraph(){
            for (int i=0; i < vertexCount; i++){
                for(auto it = graph[i]->next->begin(); it != graph[i]->next->end(); it++){
                    free((*it).second);
                }
                delete graph[i]->next;
                free(graph[i]);
            }
            free(graph);
        }

        void insertFlowEdge(int fromVertex, int toVertex, bool directed, int capacity){
            edge* e = (edge*)malloc(sizeof(edge));
            e->vertex = toVertex;
            e->capacity = capacity;
            e->currentFlow = 0;
            e->residualCapacity = capacity;
            graph[fromVertex]->next->insert(std::pair<int,edge*>(toVertex,e));
            if (!directed){
                insertFlowEdge(toVertex, fromVertex, true, capacity);
            }
        }

        void readFlowGraph(){
            initializeGraph();
            for (std::vector<Vertex>::iterator it = sources->begin(); it != sources->end(); it++) {
                insertFlowEdge(vertexCount - 2, (*it).to_int() - 1, directed, INT_MAX);
            }
            for (std::vector<Vertex>::iterator it = sinks->begin(); it != sinks->end(); it++) {
                insertFlowEdge((*it).to_int() - 1, vertexCount - 1, directed, INT_MAX);
            }

            for (RotationIterator it = sourceGraph->begin(); it != sourceGraph->end(); it++) {
                Rotation& r = *it;
                for (IncidenceIterator iit = r.begin(); iit != r.end(); iit++) {
                    Incidence& i = *iit;
                    if(r.v()==i.e().v1()) {
                        int capacity = capacities->get(i.e());
                        insertFlowEdge(i.e().v1().to_int() - 1, i.e().v2().to_int() - 1, directed, capacity);
                    }
                }
            }
        }

        void addResidualEdges(){
            for (int i=0; i<vertexCount; i++) {
                for (const auto& [number, e] : *graph[i]->next) {
                    if(graph[e->vertex]->next->find(i) == graph[e->vertex]->next->end()){
                        insertFlowEdge(e->vertex, i, true, 0);
                    }
                }
            }
        }

        void initializeSearch(){
            for (int i=0; i<vertexCount; i++) {
                discoveredVertices[i] = false;
                parentVertex[i] = -1;
            }
        }

        void bfs(int start){
            std::queue<int> q;
            q.push(start);
            discoveredVertices[start] = true;

            while (!q.empty()) {
                int removedVertex = q.front();
                q.pop();

                for (const auto& [vertexNumber, edge] : *graph[removedVertex]->next) {
                    if ((discoveredVertices[vertexNumber] == false) && (edge->residualCapacity > 0)) {
                        q.push(vertexNumber);
                        discoveredVertices[vertexNumber] = true;
                        parentVertex[vertexNumber] = removedVertex;
                    }
                }
            }
        }

    public:
        int maxFlow(){
            int source = vertexCount - 2;
            int sink = vertexCount - 1;

            readFlowGraph();
            addResidualEdges();
            initializeSearch();
            bfs(source);
            int volume = pathVolume(source, sink, parentVertex);

            while (volume > 0) {
                augmentPath(source, sink, parentVertex, volume);
                initializeSearch();
                bfs(source);
                volume = pathVolume(source, sink, parentVertex);
            }

            int flow = 0;
            for (const auto& [vertexNumber, edge] : *graph[source]->next) {
                flow += edge->currentFlow;
            }

            return flow;
        }

        EdmondsKarp(Graph* sourceGraph, std::vector<Vertex>* sources, std::vector<Vertex>* sinks, EdgeLabeling<int>* capacities, bool directed){
            this->sources = sources;
            this->sinks = sinks;
            this->capacities = capacities;
            this->sourceGraph = sourceGraph;
            this->directed = directed;
            vertexCount = sourceGraph->size() + 2;
            discoveredVertices = (bool*) calloc(vertexCount, sizeof(bool));
            parentVertex = (int*) calloc(vertexCount, sizeof(int));
        }

        ~EdmondsKarp(){
            destroyGraph();
            free(discoveredVertices);
            free(parentVertex);
        }
    };


}

#endif //BA_GRAPH_EDMONDS_KARP_HPP
